﻿using System;

namespace Checkout
{
    public class Rule
    {
        public char Item { get; set; }
        public int UnitPrice { get; set; }
        public String SpecialPrice { get; set; }
    }
}