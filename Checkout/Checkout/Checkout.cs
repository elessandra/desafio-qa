﻿using System.Collections.Generic;

namespace Checkout
{
    public class Checkout
    {
        public int Total { get; set; }
        private List<Rule> rules = new List<Rule>();
        private Dictionary<char, int> bagQuantities = new Dictionary<char, int>();
        private Dictionary<char, int> bagPrices = new Dictionary<char, int>();

        public Checkout(List<Rule> rules)
        {
            this.rules = rules;
            Total = 0;
            foreach (var rule in rules)
            {
                bagQuantities[rule.Item] = 0;
                bagPrices[rule.Item] = 0;
            }
        }

        public void Scan(char item)
        {
            foreach (var rule in rules)
            {
                if (rule.Item.Equals(item))
                {
                    bagQuantities[item] += 1;
                    if (!rule.SpecialPrice.Equals(""))
                    {
                        calculaPrecoEspecial(rule);
                    }
                    else
                    {
                        bagPrices[item] += rule.UnitPrice;
                    }
                    break;
                }
            }
            Total += bagPrices[item];
        }

        private void calculaPrecoEspecial(Rule rule)
        {
            //obtém-se o preço e a quantidade envolvidos na regra de preço especial 
            string[] aux = rule.SpecialPrice.Split('/');

            //se o número de itens for igual ao número de itens do preço especial   
            if (int.Parse(aux[0]).Equals(bagQuantities[rule.Item]))
            {
                //obtém-se o preço especial
                bagPrices[rule.Item] = int.Parse(aux[1]);
                //Desconta-se do valor Total da compra o valor anterior para o Item (quantidade x preço unitário)
                Total -= (bagQuantities[rule.Item] - 1) * rule.UnitPrice;
            }
            //se o número de itens for múltiplo do número de itens do preço especial
            else if ((bagQuantities[rule.Item] % int.Parse(aux[0])).Equals(0))
            {
                //obtém-se o preço especial multiplicado pela quantidade de vezes que o preço especial ocorre
                bagPrices[rule.Item] = int.Parse(aux[1]) * (bagQuantities[rule.Item] / int.Parse(aux[0]));
                /*
                    Desconta-se do valor Total da compra o valor anterior para o Item 
                    (quantidade x preço especial e quantidade x preço unitário)
                */
                Total -= ((bagQuantities[rule.Item] - 1) / int.Parse(aux[0]) * int.Parse(aux[1])) +
                    (bagQuantities[rule.Item] - 1) % int.Parse(aux[0]) * rule.UnitPrice;
            }
            else
            {
                //Item recebe o preço unitário
                bagPrices[rule.Item] = rule.UnitPrice;
            }

        }

    }
}
