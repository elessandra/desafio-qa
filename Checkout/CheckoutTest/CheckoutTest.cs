﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

/*
    Implementado a partir de TDD
*/

namespace CheckoutTest
{
    [TestClass]
    public class CheckoutTest
    {
        private List<Checkout.Rule> rules = new List<Checkout.Rule>();

        private void initialize()
        {
            //Parti do prinípio de que vou receber as regras neste formato para manipulá-las
            Checkout.Rule rule1 = new Checkout.Rule();
            rule1.Item = 'A';
            rule1.UnitPrice = 50;
            rule1.SpecialPrice = "3/130";

            Checkout.Rule rule2 = new Checkout.Rule();
            rule2.Item = 'B';
            rule2.UnitPrice = 30;
            rule2.SpecialPrice = "2/45";

            Checkout.Rule rule3 = new Checkout.Rule();
            rule3.Item = 'C';
            rule3.UnitPrice = 20;
            rule3.SpecialPrice = "";

            Checkout.Rule rule4 = new Checkout.Rule();
            rule4.Item = 'D';
            rule4.UnitPrice = 15;
            rule4.SpecialPrice = "";

            this.rules.Add(rule1);
            this.rules.Add(rule2);
            this.rules.Add(rule3);
            this.rules.Add(rule4);
        }       

        private int price(String goods)
        {
            Checkout.Checkout co = new Checkout.Checkout(this.rules);
            for (int i = 0; i < goods.Length; i++)
            {
                co.Scan(goods.ElementAt(i));
            }
            return co.Total;
        }

        [TestMethod]
        public void TesteDeValorTotalDeItensComprados()
        {
            initialize();
            Assert.AreEqual(0, price(""));
            Assert.AreEqual(50, price("A"));
            Assert.AreEqual(80, price("AB"));
            Assert.AreEqual(115, price("CDBA"));
            Assert.AreEqual(100, price("AA"));
            Assert.AreEqual(130, price("AAA"));
            Assert.AreEqual(180, price("AAAA"));
            Assert.AreEqual(230, price("AAAAA"));
            Assert.AreEqual(260, price("AAAAAA"));
            Assert.AreEqual(160, price("AAAB"));
            Assert.AreEqual(175, price("AAABB"));
            Assert.AreEqual(190, price("AAABBD"));
            Assert.AreEqual(190, price("DABABA"));
            Assert.AreEqual(450, price("DABABAAAAAAA"));
            Assert.AreEqual(115, price("CDBA"+""));
            Assert.AreEqual(390, price("AAAAAAAAA"));
        }

        [TestMethod]
        public void TesteIncrementalDeItensComprados()
        {
            initialize();
            Checkout.Checkout co = new Checkout.Checkout(rules);
            Assert.AreEqual(0, co.Total);
            co.Scan('A'); Assert.AreEqual(50, co.Total);
            co.Scan('B'); Assert.AreEqual(80, co.Total);
            co.Scan('A'); Assert.AreEqual(130, co.Total);
            co.Scan('A'); Assert.AreEqual(160, co.Total);
            co.Scan('B'); Assert.AreEqual(175, co.Total);
            co.Scan('B'); Assert.AreEqual(205, co.Total);
            co.Scan('C'); Assert.AreEqual(225, co.Total);
            co.Scan('A'); Assert.AreEqual(275, co.Total);
            co.Scan('B'); Assert.AreEqual(290, co.Total);
        }        
    }
}
