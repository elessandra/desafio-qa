
# language: pt
Funcionalidade Listar contatos
    Esta funcionalidade refere-se à listagem dos contatos do usuário
 
Cenário: Listar todos os contatos do usuário
    Dado que estou na tela do menu Contatos
    Então devo visualizar uma lista em ordem alfabética com todos os contatos que estão utilizando o WhatsApp, com foto, nome, status e o tipo de dispositivo que utiliza
    Se o contato não tiver foto
    Então devo visualizar uma imagem padrão   
    Se o contato não tiver o status definido
    Então o status deve permanecer sem conteúdo
    Quando rolar a lista
    Então devo visualizar os contatos conforme a rolagem para cima ou para baixo    
    Quando acabar de listar os contatos que já utilizam o WhatsApp
    Então devo visualizar na lista os contatos que ainda não utilizam o WhatsApp com foto e número do telefone
    Se o contato não tiver foto
    Então devo visualizar a mesma imagem padrão
    E devo ter a opção de convidá-lo para utilizar o WhatsApp

Funcionalidade Mostrar conversa
    Esta funcionalidade refere-se a mostrar a conversa do usuário com outro usuário
 
Cenário: Mostrar a conversa realizada entre dois usuários
    Dado que estou na tela do menu Conversas
    Quando selecionar uma conversa na lista de conversas
    Então devo visualizar as mensagens que fazem parte da conversa na tela
    E visualizá-las das mais recentes para as mais antigas
    Quando rolar a conversa
    Então as mensagens devem ser mostradas na tela de acordo com a rolagem
    Se rolar para baixo
    Então as mensagens anteriores a que está no topo da tela devem ser mostradas
    Se rolar para cima
    Então as mensagens posteriores a que está no rodapé da tela devem mostradas
   